# Edge Fixes and Workarounds



With the new update of edge, Microsoft disabled IE, However since IE's functions are core for Windows it was just disabled.\
I have gathered and listed all the possible fixes below.


## Method 1-CMD Function

The following can be used incase you need IE to test on, This one needs
- Powershell enabled
- CMD access

Run CMD and run the following command

powershell.exe -Command "(New-Object -ComObject InternetExplorer.Application).Visible = $true"


## Method 2-IE Hidden icon fix

The following can be used on any windows machine, It calls hidden items which IE is one of them since it's still on every machine.
- [Download the IEIcon file](https://gitlab.com/ps.hussam.odeh1/edge-fixes-and-workarounds/-/raw/main/Method%202/IE?inline=false)
- Enable Extensions on windows by using the following screenshot ![How to enable it](https://support.content.office.net/en-us/media/046a19de-0421-1b38-9499-4bf45ccae676.png)
- [Edit the Extension and add .URL](https://gitlab.com/ps.hussam.odeh1/edge-fixes-and-workarounds/-/raw/main/Method%202/1.png)

The icon will change into a globe icon and it will run IE.


## Method 3-Enabling Enterprise Mode

This method should be what we give to our users considering some users are not technical, Method 2 should be used instead of this one
If they are facing issues with Policies or have a hard time adding them in.

- [Download the 64-bit policy provided from Microsoft](https://msedge.sf.dl.delivery.mp.microsoft.com/filestreamingservice/files/3f0d42c6-0a02-4b1f-95cf-b904df0d76d9/MicrosoftEdgePolicyTemplates.cab) Using this Accepts Microsoft license agreement
- The File downloaded needs to be extracted
- Path to C:\Windows\PolicyDefinitions
- Check the folder's name for the language used ![Language](https://gitlab.com/ps.hussam.odeh1/edge-fixes-and-workarounds/-/raw/main/Method%203/1.png)
  - If the PC has multiple files the one mainly used should be used
- Copy from the extracted folder MicrosoftEdgePolicyTemplates\windows The 3 files .admx and the language folder and paste them in C:\Windows\PolicyDefinitions
- [Download the EnterpriseList.xml](https://gitlab.com/ps.hussam.odeh1/edge-fixes-and-workarounds/-/raw/main/Method%203/EnterpriseList.xml?inline=false) And paste it in any convenient path
  - During this step the XML File should be edited
  - Open the EnterpriseList.xml and Change the URL to the desired one (This should be used for the whole site not just one page)
- Go to Group Policy Edit
  - CMD > gpedit
  - Start menu > Group Policy Edit
  - Start menu > MMC > file > Add/Remove Snap-in > Group Policy Object > Add
- Go to Administrative Templates > Microsoft Edge
  - Configure Internet Explorer Integration > Enabled > Internet Explorer mode
  - Configure the Enterprise Mode Site List > Enabled > Insert the path of EnterpriseList.xml
  - Apply both changes
- At this point if Edge is Opened it needs to be closed
- In Edge use the URL Bar and Use Edge://compat
  - Force Update
 
With Enterprise mode some of us might want to use F12 Or Developer Tools. Those tools are disabled in those mode of Enterprise Edge
To enable them we have to run the following on a Run window, in this mode we can also select all the different versions of IE if we didn't
add them in the XML file that we edited prior to this step

- %systemroot%\system32\f12\IEChooser.exe
- Select the window that you need it to open
 
Some users might faces issues with this method due to conflicting policies in those cases we can use
Method 2 or 1 depending on the situation.
